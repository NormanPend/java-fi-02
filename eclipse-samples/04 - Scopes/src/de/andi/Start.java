package de.andi;

public class Start {

	static int is=666; //Statischer (globaler) Scope
	
	public static void main(String[] args) {
		int i=100;  //lokaler Scope
		is=333;
		
		{  //Block Begin
			int x=5555;
		}  //Block End
		
		//x=5; Falsch, da x tot
		
	}
	
	static void x() {
		//i=200;
		is=222;
	}

}
